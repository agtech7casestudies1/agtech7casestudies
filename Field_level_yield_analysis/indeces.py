# !python
# cython: language_level=3
import rasterio
import matplotlib.pyplot as plt
import numpy as np

def ndvi(red, nir,path):
  with rasterio.open(red) as src:
    red = src.read(1, out_shape=(1, int(src.height), int(src.width)))
  with rasterio.open(nir) as src:
    nir = src.read(1, out_shape=(1, int(src.height), int(src.width)))
  nir = nir.astype('f4')
  red = red.astype('f4')
  ndvi = (nir - red) / (nir + red)
  with rasterio.open(path,'w',
                    driver='GTiff',
                    height=ndvi.shape[0],
                    width=ndvi.shape[1],
                    count=1,
                    dtype=ndvi.dtype,
                    crs=src.crs,
                    transform=src.transform,
                    nodata=0,) as dst:
    dst.write(ndvi,1)
    dst.close()

def evi(blue,red,nir,path):
  with rasterio.open(blue) as src:
    blue = src.read(1, out_shape=(1, int(src.height), int(src.width)))
  with rasterio.open(red) as src:
    red = src.read(1, out_shape=(1, int(src.height), int(src.width)))
  with rasterio.open(nir) as src:
    nir = src.read(1, out_shape=(1, int(src.height), int(src.width)))
  nir = nir.astype('f4')
  red = red.astype('f4')
  blue=blue.astype('f4')
  evi = 2.5* ((nir-red)/((nir+6*red-7.5*blue)+1))
  with rasterio.open(path,'w',
                    driver='GTiff',
                    height=evi.shape[0],
                    width=evi.shape[1],
                    count=1,
                    dtype=evi.dtype,
                    crs=src.crs,
                    transform=src.transform,
                    nodata=0,) as dst:
    dst.write(evi,1)
    dst.close()
    
def nrvi(red,nir,path):
  with rasterio.open(red) as src:
    red = src.read(1, out_shape=(1, int(src.height), int(src.width)))
  with rasterio.open(nir) as src:
    nir = src.read(1, out_shape=(1, int(src.height), int(src.width)))
  nir = nir.astype('f4')
  red = red.astype('f4')
  rvi = red/nir
  nrvi = (rvi-1)/(rvi+1)
  with rasterio.open(path,'w',
                      driver='GTiff',
                      height=nrvi.shape[0],
                      width=nrvi.shape[1],
                      count=1,
                      dtype=nrvi.dtype,
                      crs=src.crs,
                      transform=src.transform,
                      nodata=0,) as dst:
    dst.write(nrvi,1)
    dst.close()

def ipvi(red, nir,path):
  with rasterio.open(red) as src:
    red = src.read(1, out_shape=(1, int(src.height), int(src.width)))
  with rasterio.open(nir) as src:
    nir = src.read(1, out_shape=(1, int(src.height), int(src.width)))
  nir = nir.astype('f4')
  red = red.astype('f4')
  ipvi = nir/ (nir + red)
  with rasterio.open(path,'w',
                     driver='GTiff',
                      height=ipvi.shape[0],
                      width=ipvi.shape[1],
                      count=1,
                      dtype=ipvi.dtype,
                      crs=src.crs,
                      transform=src.transform,
                      nodata=0,) as dst:
    dst.write(ipvi,1)
    dst.close()

def savi(red, nir,path):
  with rasterio.open(red) as src:
    red = src.read(1, out_shape=(1, int(src.height), int(src.width)))
  with rasterio.open(nir) as src:
    nir = src.read(1, out_shape=(1, int(src.height), int(src.width)))
  nir = nir.astype('f4')
  red = red.astype('f4')
  savi = ((nir-red)/(nir + red + 0.428))*(1+0.428)
  with rasterio.open(path,'w',
                     driver='GTiff',
                      height=savi.shape[0],
                      width=savi.shape[1],
                      count=1,
                      dtype=savi.dtype,
                      crs=src.crs,
                      transform=src.transform,
                      nodata=0,) as dst:
    dst.write(savi,1)
    dst.close()

def msavi2(red, nir,path):
  with rasterio.open(red) as src:
    red = src.read(1, out_shape=(1, int(src.height), int(src.width)))
  with rasterio.open(nir) as src:
    nir = src.read(1, out_shape=(1, int(src.height), int(src.width)))
  nir = nir.astype('f4')
  red = red.astype('f4')
  msavi2 = (2*nir+1-np.sqrt((2*nir+1)**2 -8*(nir-red)))/2
  with rasterio.open(path,'w',
                     driver='GTiff',
                      height=msavi2.shape[0],
                      width=msavi2.shape[1],
                      count=1,
                      dtype=msavi2.dtype,
                      crs=src.crs,
                      transform=src.transform,
                      nodata=0,) as dst:
    dst.write(msavi2,1)
    dst.close()

def gemi(red, nir,path):
  with rasterio.open(red) as src:
    red = src.read(1, out_shape=(1, int(src.height), int(src.width)))
  with rasterio.open(nir) as src:
    nir = src.read(1, out_shape=(1, int(src.height), int(src.width)))
  nir = nir.astype('f4')
  red = red.astype('f4')
  n=(2*(nir*nir-red*red)+1.5*nir+0.5*red)/(nir+red+0.5)
  gemi = n*(1-0.25*n)-((red-0.125)/(1-red))
  with rasterio.open(path,'w',
                     driver='GTiff',
                      height=gemi.shape[0],
                      width=gemi.shape[1],
                      count=1,
                      dtype=gemi.dtype,
                      crs=src.crs,
                      transform=src.transform,
                      nodata=0,) as dst:
    dst.write(gemi,1)
    dst.close()

def tvi(red, nir,path):
  with rasterio.open(red) as src:
    red = src.read(1, out_shape=(1, int(src.height), int(src.width)))
  with rasterio.open(nir) as src:
    nir = src.read(1, out_shape=(1, int(src.height), int(src.width)))
  nir = nir.astype('f4')
  red = red.astype('f4')
  tvi = np.sqrt(((nir-red)/(nir+red))+0.5)
  with rasterio.open(path,'w',
                     driver='GTiff',
                      height=tvi.shape[0],
                      width=tvi.shape[1],
                      count=1,
                      dtype=tvi.dtype,
                      crs=src.crs,
                      transform=src.transform,
                      nodata=0,) as dst:
    dst.write(tvi,1)
    dst.close() 

def ttvi(red, nir,path):
  with rasterio.open(red) as src:
    red = src.read(1, out_shape=(1, int(src.height), int(src.width)))
  with rasterio.open(nir) as src:
    nir = src.read(1, out_shape=(1, int(src.height), int(src.width)))
  nir = nir.astype('f4')
  red = red.astype('f4')
  ndvi=(nir-red)/(nir+red)
  ttvi = np.sqrt(abs(ndvi)+0.5)
  with rasterio.open(path,'w',
                     driver='GTiff',
                      height=ttvi.shape[0],
                      width=ttvi.shape[1],
                      count=1,
                      dtype=ttvi.dtype,
                      crs=src.crs,
                      transform=src.transform,
                      nodata=0,) as dst:
    dst.write(ttvi,1)
    dst.close()

def ctvi(red, nir,path):
  with rasterio.open(red) as src:
    red = src.read(1, out_shape=(1, int(src.height), int(src.width)))
  with rasterio.open(nir) as src:
    nir = src.read(1, out_shape=(1, int(src.height), int(src.width)))
  nir = nir.astype('f4')
  red = red.astype('f4')
  ctvi = ((((nir-red)/(nir+red))+0.5)/(abs((nir-red)/(nir+red))+0.5))*np.sqrt((abs((nir-red)/(nir+red))+0.5))
  with rasterio.open(path,'w',
                     driver='GTiff',
                      height=ctvi.shape[0],
                      width=ctvi.shape[1],
                      count=1,
                      dtype=ctvi.dtype,
                      crs=src.crs,
                      transform=src.transform,
                      nodata=0,) as dst:
    dst.write(ctvi,1)
    dst.close()

def arvi(blue,red,nir,path):
  with rasterio.open(blue) as src:
    blue = src.read(1, out_shape=(1, int(src.height), int(src.width)))
  with rasterio.open(red) as src:
    red = src.read(1, out_shape=(1, int(src.height), int(src.width)))
  with rasterio.open(nir) as src:
    nir = src.read(1, out_shape=(1, int(src.height), int(src.width)))
  nir = nir.astype('f4')
  red = red.astype('f4')
  blue=blue.astype('f4')
  rb=red-(blue-red)
  arvi = (nir-rb)/(nir+rb)
  with rasterio.open(path,'w',
                    driver='GTiff',
                    height=arvi.shape[0],
                    width=arvi.shape[1],
                    count=1,
                    dtype=arvi.dtype,
                    crs=src.crs,
                    transform=src.transform,
                    nodata=0,) as dst:
    dst.write(arvi,1)
    dst.close()

def gci(green,nir,path):
  with rasterio.open(green) as src:
    green = src.read(1, out_shape=(1, int(src.height), int(src.width)))
  with rasterio.open(nir) as src:
    nir = src.read(1, out_shape=(1, int(src.height), int(src.width)))
  nir = nir.astype('f4')
  green = green.astype('f4')
  gci=(nir/green)-1
  with rasterio.open(path,'w',
                     driver='GTiff',
                      height=gci.shape[0],
                      width=gci.shape[1],
                      count=1,
                      dtype=gci.dtype,
                      crs=src.crs,
                      transform=src.transform,
                      nodata=0,) as dst:
    dst.write(gci,1)
    dst.close()

def dvi(red,nir,path):
  with rasterio.open(red) as src:
    red = src.read(1, out_shape=(1, int(src.height), int(src.width)))
  with rasterio.open(nir) as src:
    nir = src.read(1, out_shape=(1, int(src.height), int(src.width)))
  nir = nir.astype('f4')
  red = red.astype('f4')
  dvi = nir - red
  with rasterio.open(path,'w',
                     driver='GTiff',
                      height=dvi.shape[0],
                      width=dvi.shape[1],
                      count=1,
                      dtype=dvi.dtype,
                      crs=src.crs,
                      transform=src.transform,
                      nodata=0,) as dst:
    dst.write(dvi,1)
    dst.close()



def osavi(red,nir,path):
  with rasterio.open(red) as src:
    red = src.read(1, out_shape=(1, int(src.height), int(src.width)))
  with rasterio.open(nir) as src:
    nir = src.read(1, out_shape=(1, int(src.height), int(src.width)))
  nir = nir.astype('f4')
  red = red.astype('f4')
  osavi = 1.5 * (nir - red) / (nir + red + 0.16)
  with rasterio.open(path,'w',
                     driver='GTiff',
                      height=osavi.shape[0],
                      width=osavi.shape[1],
                      count=1,
                      dtype=osavi.dtype,
                      crs=src.crs,
                      transform=src.transform,
                      nodata=0,) as dst:
    dst.write(osavi,1)
    dst.close()

def gndvi(green,nir,path):
  with rasterio.open(green) as src:
    green = src.read(1, out_shape=(1, int(src.height), int(src.width)))
  with rasterio.open(nir) as src:
    nir = src.read(1, out_shape=(1, int(src.height), int(src.width)))
  nir = nir.astype('f4')
  green = green.astype('f4')
  gndvi = (nir - green) / (nir + green)
  with rasterio.open(path,'w',
                     driver='GTiff',
                      height=gndvi.shape[0],
                      width=gndvi.shape[1],
                      count=1,
                      dtype=gndvi.dtype,
                      crs=src.crs,
                      transform=src.transform,
                      nodata=0,) as dst:
    dst.write(gndvi,1)
    dst.close()

def cvi(green,red,nir,path):
  with rasterio.open(green) as src:
    green = src.read(1, out_shape=(1, int(src.height), int(src.width)))
  with rasterio.open(red) as src:
    red = src.read(1, out_shape=(1, int(src.height), int(src.width)))
  with rasterio.open(nir) as src:
    nir = src.read(1, out_shape=(1, int(src.height), int(src.width)))
  nir = nir.astype('f4')
  red = red.astype('f4')
  green=green.astype('f4')
  cvi = (nir*red)/(green*green)
  with rasterio.open(path,'w',
                    driver='GTiff',
                    height=cvi.shape[0],
                    width=cvi.shape[1],
                    count=1,
                    dtype=cvi.dtype,
                    crs=src.crs,
                    transform=src.transform,
                    nodata=0,) as dst:
    dst.write(cvi,1)
    dst.close()
