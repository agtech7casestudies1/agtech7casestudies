# !python
# cython: language_level=3
import rasterio
from rasterio import mask as msk
import fiona
import matplotlib.pyplot as plt
import numpy as np
import geopandas as gpd
import pandas as pd

def extract_by_mask(vector, raster, path):
  #Read Shapefile (polygon)
  with fiona.open(vector, "r") as shapefile:
    shapes = [feature["geometry"] for feature in shapefile]
  #Read TIFF file
  with rasterio.open(raster) as src:
    out_image, out_transform = msk.mask(src, shapes, crop=True)
    out_meta = src.meta
  #Save extracted image
  out_meta.update({"driver": "GTiff",
                    "height": out_image.shape[1],
                    "width": out_image.shape[2],
                    "transform": out_transform})
  with rasterio.open(path, "w", **out_meta) as dest:
    dest.write(out_image)

def index_to_list(index):
  indeks=index.read(1).tolist()
  lista=[]
  for i in indeks:
    for j in i:
      lista.append(j)
  return lista


def read_index(index):
  with rasterio.Env():
      with rasterio.open(index) as src:
          crs = src.crs

          # create 1D coordinate arrays (coordinates of the pixel center)
          xmin, ymax = np.around(src.xy(0.00, 0.00), 9)  # src.xy(0, 0)
          xmax, ymin = np.around(src.xy(src.height-1, src.width-1), 9)  # src.xy(src.width-1, src.height-1)
          x = np.linspace(xmin, xmax, src.width)
          y = np.linspace(ymax, ymin, src.height)  # max -> min so coords are top -> bottom



          # create 2D arrays
          xs, ys = np.meshgrid(x, y)
          zs = src.read(1)

          # Apply NoData mask
          mask = src.read_masks(1) > 0
          xs, ys, zs = xs[mask], ys[mask], zs[mask]

  data = {"X": pd.Series(xs.ravel()),
          "Y": pd.Series(ys.ravel()),
          "index": pd.Series(zs.ravel())}

  df = pd.DataFrame(data=data)
  geometry = gpd.points_from_xy(df.X, df.Y)
  gdf = gpd.GeoDataFrame(df, crs=crs, geometry=geometry)

  return gdf

def read_index2(index,naziv):
  crs = index.crs
  name=str(naziv)

  # create 1D coordinate arrays (coordinates of the pixel center)
  xmin, ymax = np.around(index.xy(0.00, 0.00), 9)  
  xmax, ymin = np.around(index.xy(index.height-1, index.width-1), 9)  
  x = np.linspace(xmin, xmax, index.width)
  y = np.linspace(ymax, ymin, index.height)

  # create 2D arrays
  xs, ys = np.meshgrid(x, y)
  zs = index.read(1)

  # Apply NoData mask
  mask = index.read_masks(1) > 0
  xs, ys, zs = xs[mask], ys[mask], zs[mask]

  data = {"X": pd.Series(xs.ravel()),
          "Y": pd.Series(ys.ravel()),
          name: pd.Series(zs.ravel())}

  df = pd.DataFrame(data=data)
  df.dropna(inplace=True)
  geometry = gpd.points_from_xy(df.X, df.Y)
  gdf = gpd.GeoDataFrame(df, crs=crs, geometry=geometry)

  return gdf
