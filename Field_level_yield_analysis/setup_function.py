from distutils.core import setup
from distutils.extension import Extension
from Cython.Distutils import build_ext

ext_modules = [
    Extension("function",  ["function.py"]),
#   ... all your modules that need be compiled ...
]
setup(
    name = 'compiled_code',
    cmdclass = {'build_ext': build_ext},
    ext_modules = ext_modules
)